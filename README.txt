This is a utility module which helps to put an updated version of content
in a draft-mode.The updated content will not be published until and unless 
content editor revert this version.

------------------------------
Enable the module from module->Live Node Revision.
Edit a content type -> In "publishing options" section check the box
"Revert Original Node".
Structure -> Trigger-> After saving updated content->
Choose action "Action-to-revert-node:".
